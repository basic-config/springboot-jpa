package net.java.springbootjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 자체 실행
 */
//@Configuration
//@ComponentScan
//@EnableAutoConfiguration
//public class WebApplication {
//    public static void main(String[] args) {
//        SpringApplication.run(WebApplication.class, args);
//    }
//}

/**
 * 외부 server와연결
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class WebApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }
}