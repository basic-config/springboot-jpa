package net.java.springbootjpa.user.controller;

import net.java.springbootjpa.user.dao.UserRepository;
import net.java.springbootjpa.user.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by myborn on 2016-12-18.
 */
@Controller
@RequestMapping("/member")
public class UserController {
    @Autowired
    private UserRepository memberDao;

    @RequestMapping("/list")
    public String list(Model model) {
        List<UserVO> list = memberDao.findAll();
        model.addAttribute("list", list);
        return "list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return "add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String postAdd(UserVO member) {
        memberDao.save(member);
        return "redirect:/member/list";
    }

    @RequestMapping(value = "/mod/{id}", method = RequestMethod.GET)
    public String mod(@PathVariable String id, Model model) {
        UserVO member = memberDao.findOne(id);
        model.addAttribute("data", member);
        return "mod";
    }

    @RequestMapping(value = "/mod", method = RequestMethod.POST)
    public String postMod(UserVO member) {
        memberDao.save(member);
        return "redirect:/member/list";
    }

    @RequestMapping("/del/{id}")
    public String del(@PathVariable String id) {
        memberDao.delete(id);
        return "redirect:/member/list";
    }
}
