package net.java.springbootjpa.user.dao;

import net.java.springbootjpa.user.vo.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by myborn on 2016-12-18.
 */
public interface UserRepository extends JpaRepository<UserVO, String> {
}
